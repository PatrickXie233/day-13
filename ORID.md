**O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?**

1. codeReview:我的布局出了问题，在家用 2k 屏调的然后到公司就出现异常
2. concept map:绘制了我们这周学习的 concept map，感觉小组氛围比前几个星期要好很多
3. todoList back-end: 回顾并练习了上周的 spring boot 知识，在做作业的时候感觉没有生疏，挺流畅的
4. session：小组演讲中出现沟通不够，出现内容重复，每个人的时间没控制好

**R (Reflective): Please use one word to express your feelings about today's class.**

充实

**I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?**

用 todolist 这个简单的 case 让我们实践了前后端交互的流程，感觉离全栈工程师又近了一步

**D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?**

css 还不是很熟练，还是得多加练习，在作业做完后要多检查，有好多问题都是没检查导致的
