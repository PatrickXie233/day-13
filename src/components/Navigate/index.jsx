import { useNavigate } from 'react-router-dom'
import './index.css'
import { useState } from 'react'
import type { MenuProps } from 'antd'
import { Menu } from 'antd'
const Navigate = () => {
    const navigate = useNavigate()
    const [current, setCurrent] = useState('homepage')
    const items: MenuProps['items'] = [
        {
            label: 'homepage',
            key: '',
        },
        {
            label: 'done list',
            key: 'donelist',
        },
        {
            label: 'help',
            key: 'help',
        },
    ]

    const onClick: MenuProps['onClick'] = (e) => {
        setCurrent(e.key)
        navigate(`/${e.key}`)
    }
    return (
        <Menu
            onClick={onClick}
            selectedKeys={[current]}
            mode="horizontal"
            items={items}
        />
    )
}

export default Navigate
