import { useSelector } from 'react-redux'
import './index'
import { useNavigate } from 'react-router-dom'

import { List, Card, message, Button } from 'antd'
import { CheckCircleOutlined } from '@ant-design/icons'
import './index.css'
import useTodos from '../../Hooks/useTodos.js'

const DoneListPage = () => {
    // dependency
    const doneList = useSelector((state) => state.todoList.todoList).filter(
        (item) => item.done,
    )
    const todos = useTodos()
    const [messageApi, contextHolder] = message.useMessage()
    const navigate = useNavigate()

    // method
    const handleClick = (id) => {
        navigate(`/donelist/${id}`)
    }
    const handleDone = ({ id, done, text }) => {
        todos.putTodoListItemHook(id, {
            done: !done,
            text: text,
        })
        messageApi.open({
            key: 'updatable',
            type: 'success',
            content: 'update success',
        })
    }
    return (
        <div className="">
            <h1>done list</h1>
            {contextHolder}
            <List
                itemLayout="vertical"
                grid={{
                    gutter: 16,
                    xs: 1,
                    sm: 2,
                    md: 4,
                    lg: 4,
                    xl: 6,
                    xxl: 4,
                }}
                dataSource={doneList}
                renderItem={(item, index) => (
                    <List.Item>
                        <Card
                            className="card"
                            title="Todo Item"
                            extra={
                                <Button
                                    type="link"
                                    onClick={() => handleClick(item.id)}
                                >
                                    detail
                                </Button>
                            }
                        >
                            <div
                                onClick={() => handleDone(item)}
                                className="context"
                            >
                                <span className="text">{item.text}</span>
                                <CheckCircleOutlined
                                    className={
                                        item.done
                                            ? 'finish-icon icon float'
                                            : 'icon float'
                                    }
                                />
                            </div>
                        </Card>
                    </List.Item>
                )}
            />
        </div>
    )
}
export default DoneListPage
