import { useEffect } from 'react'
import getTodoList from '../../api'
import TodoList from '../../components/TodoList'

const Home = () => {
    return (
        <div>
            <TodoList> </TodoList>
        </div>
    )
}
export default Home
