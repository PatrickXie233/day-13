import { Outlet } from 'react-router-dom'
import './App.css'
import Navigate from './components/Navigate'
import { Content, Header } from 'antd/es/layout/layout'
function App() {
    return (
        <div className="App">
            <Header className="header">
                <Navigate></Navigate>
            </Header>
            <Content className="content">
                <Outlet></Outlet>
            </Content>
        </div>
    )
}

export default App
